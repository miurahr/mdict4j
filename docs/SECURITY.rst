Security Policy
===============

Supported Versions
------------------

+---------+---------------------+
| Version | Status              |
+=========+=====================+
| 0.5.x   | Development version |
+---------+---------------------+
| < 0.5   | not supported       |
+---------+---------------------+

Reporting a Vulnerability
-------------------------

Please disclose security vulnerabilities privately at miurahr@linux.com in English or Japanese, no Chinese.