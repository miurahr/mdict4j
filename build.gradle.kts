import java.io.FileInputStream
import java.util.Properties

plugins {
    checkstyle
    jacoco
    signing
    `java-library`
    `java-library-distribution`
    `maven-publish`
    alias(libs.plugins.spotbugs)
    alias(libs.plugins.spotless)
    alias(libs.plugins.nexus.publish)
    alias(libs.plugins.git.version) apply false
    id("tokyo.northside.sphinx") version "1.0.4"
    id("org.gradlex.extra-java-module-info") version "1.3"
}

group = "tokyo.northside"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.jetbrains.annotations)
    implementation(libs.jackson.core)
    implementation(libs.jackson.databind)
    implementation(libs.jackson.xml)
    implementation(libs.bc.prov)
    implementation("org.anarres.lzo:lzo-core:1.0.6")
    implementation("com.github.takawitter:trie4j:0.9.8")
    testImplementation(libs.jsoup)
    testImplementation("org.apache.tika:tika-core:2.6.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.9.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.2")
    testImplementation("org.apache.tika:tika-core:2.7.0")
    testRuntimeOnly("org.slf4j:slf4j-simple:2.0.3")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

extraJavaModuleInfo {
    module("com.github.takawitter:trie4j", "com.github.takawitter.trie4j") {
        exports("org.trie4j")
        exports("org.trie4j.doublearray")
        exports("org.trie4j.patricia")
    }
    module("org.anarres.lzo:lzo-core", "org.anarres.lzo") {
        exports("org.anarres.lzo")
    }
    automaticModule("com.google.code.findbugs:annotations", "com.google.code.findbugs.annotations")
    automaticModule("commons-logging:commons-logging", "org.apache.commons.logging")
}

tasks.check {
    dependsOn(tasks.jacocoTestCoverageVerification)
}

jacoco {
    toolVersion="0.8.6"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(false)
        html.required.set(true)
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

java {
    toolchain {
         languageVersion.set(JavaLanguageVersion.of(11))
    }
    withSourcesJar()
    withJavadocJar()
}

// we handle cases without .git directory
val home = System.getProperty("user.home")!!
val javaHome = System.getProperty("java.home")!!
val versionProperties = project.file("src/main/resources/version.properties")
val dotgit = project.file(".git")

if (dotgit.exists()) {
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = if (details.isCleanTag) {  // release version
        baseVersion
    } else {  // snapshot version
        baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                pom {
                    name.set("MDict4J")
                    description.set("MDict parser for java")
                    url.set("https://codeberg.org/miurahr/mdict4j")
                    licenses {
                        license {
                            name.set("The GNU General Public License, Version 3")
                            url.set("https://www.gnu.org/licenses/licenses/gpl-3.html")
                            distribution.set("repo")
                        }
                    }
                    developers {
                        developer {
                            id.set("miurahr")
                            name.set("Hiroshi Miura")
                            email.set("miurahr@linux.com")
                        }
                    }
                    scm {
                        connection.set("scm:git:git://codeberg.org/miurahr/mdict4j.git")
                        developerConnection.set("scm:git:git://codeberg.org/miurahr/mdict4j.git")
                        url.set("https://codeberg.org/miurahr/mdict4j")
                    }
                }
            }
        }
    }

    val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}
    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

    signing {
        when (signKey) {
            "signingKey" -> {
                val signingKey: String? by project
                val signingPassword: String? by project
                useInMemoryPgpKeys(signingKey, signingPassword)
            }
            "signing.keyId" -> {
                // default signatory - do nothing()
                // please set
                // signing.keyId = 0xAAAAAA
                // signing.password = "signature passphrase"
                // secretKeyRingFile = "secring.gpg"
                // e.g. gpg --export-secret-keys > secring.gpg
            }
            "signing.gnupg.keyName" -> {
                useGpgCmd()
            }
        }
        sign(publishing.publications["mavenJava"])
    }

    nexusPublishing {
        repositories{
            sonatype()
        }
    }
} else if (versionProperties.exists()) {
    version = Properties().apply { load(FileInputStream(versionProperties)) }.getProperty("version")
}

tasks.register("writeVersionFile") {
    val folder = project.file("src/main/resources")
    if (!folder.exists()) {
        folder.mkdirs()
    }
    versionProperties.delete()
    versionProperties.appendText("version=" + project.version)
}

tasks.getByName("jar") {
    dependsOn("writeVersionFile")
}

tasks.sphinx {
    sourceDirectory {"docs"}
}

tasks.jacocoTestCoverageVerification {
    dependsOn(tasks.test)
    violationRules {
        rule {
            element = "CLASS"
            includes = listOf("**.mdict.*", "**.mdict.cache.*")
            // exclude exception class and POJO
            excludes = listOf("**.mdict.MDException", "**.mdict.MDictDictionaryInfo", "**.mdict.MDictParser.?")
            limit {
                minimum = "0.5".toBigDecimal()
            }
        }
    }
}

spotless {
    format("misc") {
        target(listOf("*.gradle", ".gitignore", "*.rst"))
        trimTrailingWhitespace()
        indentWithSpaces()
        endWithNewline()
    }
    format("styling") {
        target(listOf("*.md"))
        prettier()
    }
    java {
        target(listOf("src/*/java/**/*.java"))
        palantirJavaFormat()
        importOrder()
        removeUnusedImports()
        formatAnnotations()
    }
}
