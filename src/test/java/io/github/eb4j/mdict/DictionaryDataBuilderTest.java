/*
 * MD4J, a parser library for MDict format.
 * Copyright (C) 2022 Hiroshi Miura.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.github.eb4j.mdict;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DictionaryDataBuilderTest {

    @Test
    void addAndLookup() {
        DictionaryDataBuilder<String> builder = new DictionaryDataBuilder<>();
        builder.add("key1", "value");
        builder.add("key1", "anotherValue");
        builder.add("key2", "value2");
        builder.add("key1", "value3");
        DictionaryData<String> data = builder.build();
        // data size is number of unique keys
        Assertions.assertEquals(2, data.size());
        // simple data
        Assertions.assertEquals(1, data.lookUp("key2").size());
        Assertions.assertEquals("value2", data.lookUp("key2").get(0).getValue());
        // multiple values
        Assertions.assertEquals(3, data.lookUp("key1").size());
        Assertions.assertEquals("value", data.lookUp("key1").get(0).getValue());
        Assertions.assertEquals("value3", data.lookUp("key1").get(2).getValue());
    }
}
