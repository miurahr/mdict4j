module tokyo.northside.mdict {
    exports io.github.eb4j.mdict;

    requires org.apache.commons.logging;
    requires org.jetbrains.annotations;
    requires org.anarres.lzo;
    requires org.bouncycastle.provider;
    requires com.github.takawitter.trie4j;
    requires com.google.code.findbugs.annotations;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.xml;
}
